# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# User specific environment and startup programs

PATH=$PATH:$HOME/.local/bin:$HOME/bin

export PATH

export CATALINA_HOME=/usr/share/tomcat/
export CATALINA_BASE=$CATALINA_HOME
export JAVA_HOME=/usr/bin/java
